class test {
    constructor(loop, fill) {
        this.loop = loop;
        this.fill = fill;
        this.rest = []
        this.tableid = null
    }
    compare(a, b) {
        if (a.timeTotal < b.timeTotal) {
            return -1;
        }
        if (a.timeTotal > b.timeTotal) {
            return 1;
        }
        return 0;
    }

    toHTML(end) {
        var restP = document.getElementById('restP')
        if (end) {
            document.getElementById('TestNow').classList.remove('hide')

            var loading = document.getElementById('loading');
            loading.classList.add('hide')
            loading.classList.remove('show')

            restP.classList.remove('hide')
        }


        var div = document.createElement("div")
        var table = document.createElement("table")
        var tr = document.createElement('tr');
        var th1 = document.createElement('th');
        th1.appendChild(document.createTextNode('Loop Name'))

        var th2 = document.createElement('th');
        th2.appendChild(document.createTextNode('Time Total'))

        tr.appendChild(th1)
        tr.appendChild(th2)
        table.appendChild(tr)
        var newRest = this.rest.sort(this.compare)
        newRest.forEach(el => {
            var tr = document.createElement('tr');
            var td1 = document.createElement('td');
            td1.appendChild(document.createTextNode(el.name))

            var td2 = document.createElement('td');
            td2.appendChild(document.createTextNode(el.timeTotal))

            tr.appendChild(td1)
            tr.appendChild(td2)
            table.appendChild(tr)
        })

        var h2 = document.createElement('h2');
        h2.appendChild(document.createTextNode(`ทดสอบ ${this.tableid}`))
        div.appendChild(h2)
        div.appendChild(table)

        restP.appendChild(div)
    }
    Loop(number) {
        this.tableid = number
        console.log('')
        console.log(`ทดสอบ ${number}`)
        var array = Array(this.loop).fill(this.fill)
        var performance = window.performance;


        var rest = []
        var t0 = performance.now()
        array.forEach(x => {
            rest.push(x)
        })
        var t1 = performance.now()
        this.rest.push({
            name: 'forEach',
            timeTotal: (t1 - t0)
        })
        console.log('forEach', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")


        var rest = []
        var t0 = performance.now()
        for (let index = 0; index < array.length; index++) {
            rest.push(array[index])
        }
        var t1 = performance.now()
        this.rest.push({
            name: 'for i',
            timeTotal: (t1 - t0)
        })
        console.log('for i', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")


        var rest = []
        var t0 = performance.now()
        for (index in array) {
            rest.push(array[index])
        }
        var t1 = performance.now()
        this.rest.push({
            name: 'for in',
            timeTotal: (t1 - t0)
        })
        console.log('for in', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")

        var rest = []
        var t0 = performance.now()
        for (index of array) {
            rest.push(array[index])
        }
        var t1 = performance.now()
        this.rest.push({
            name: 'for of',
            timeTotal: (t1 - t0)
        })
        console.log('for of', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")

        var rest = []
        var t0 = performance.now()
        var index = 0;
        while (index < array.length) {
            rest.push(array[index])
            index++;
        }
        var t1 = performance.now()
        this.rest.push({
            name: 'while',
            timeTotal: (t1 - t0)
        })
        console.log('while', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")

        var rest = []
        var t0 = performance.now()
        var index = 0;
        do {
            rest.push(array[index])
            index++;
        }
        while (index < array.length)
        var t1 = performance.now()
        this.rest.push({
            name: 'do while',
            timeTotal: (t1 - t0)
        })
        console.log('do while', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")


        var rest = []
        var t0 = performance.now()
        array.map((index) => {
            rest.push(array[index])
        })
        var t1 = performance.now()
        this.rest.push({
            name: 'map',
            timeTotal: (t1 - t0)
        })
        console.log('map', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")
    }
}