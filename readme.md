## Code ทดสอบจริงๆมีเท่านี้นะครับ

ทดสอบ Javascript Loop แบบต่างๆ ตัวไหนเร็วสุด
ผลการทดสอบ https://youtu.be/zkLCnIKm9T8


        var array = Array(1000000).fill(1)
        var performance = window.performance;
        var rest = []
        var t0 = performance.now()

        //สิ่งที่ต้องการทดสอบ
        array.forEach(x => {
            rest.push(x)
        })
        //สิ่งที่ต้องการทดสอบ

        var t1 = performance.now()
        this.rest.push({
            name: 'forEach',
            timeTotal: (t1 - t0)
        })

        //แสดงผลการทดสอบ
        console.log('forEach', rest.length, "ทำงาน " + (t1 - t0) + " มิลลิวินาที")
